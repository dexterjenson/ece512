/* 
Code for ECE 512 Lab 8
Date updated: 2019-05-23
For detailed documentation and explanation please visit https://gitlab.com/agm/ece512
*/

// Interrupt related 
#define SETBIT(ADDRESS,BIT) (ADDRESS |= (1<<BIT))
#define CLEARBIT(ADDRESS,BIT) (ADDRESS &= ~(1<<BIT))
#define CHECKBIT(ADDRESS,BIT) (ADDRESS & (1<<BIT))
#define TOGGLEBIT(ADDRESS,BIT) (ADDRESS ^= (1<<BIT))

//PWM related
unsigned int runDC;
#define DEADTIME    8
#define PERIOD      160   // 25kHz:320; 20kHz:400
#define DUTYRATIO   10
#define MAXTIMECOUNTDC 2000
unsigned int dutyratio;
#define DUTYRATIO_MAX  40
#define DUTYRATIO_MIN  4

// State machine and protection related
#define TRUE 1
#define FALSE 0
unsigned char switchstate[5];
unsigned int Run_state;
int climb;
#define INCREASING 1
#define DECREASING -1
#define CLIMB_STEP 4.8
