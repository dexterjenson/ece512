 // This file is associated with the file that lists the code for generating a PWM on Timer 1
 // Filename: Timer1.ino
 // Date updated: Spring-2016
 // Version 1.0

// Interrupt related 
#define SETBIT(ADDRESS,BIT) (ADDRESS |= (1<<BIT))
#define CLEARBIT(ADDRESS,BIT) (ADDRESS &= ~(1<<BIT))
#define CHECKBIT(ADDRESS,BIT) (ADDRESS & (1<<BIT))
#define TOGGLEBIT(ADDRESS,BIT) (ADDRESS ^= (1<<BIT))

//PWM related
unsigned int runDC;
#define DEADTIME    8
#define PERIOD      400   // 25kHz:320; 20kHz:400
#define DUTYRATIO   10
#define MAXTIMECOUNTDC 2000
unsigned int dutyratio;
#define DUTYRATIO_MAX  40
#define DUTYRATIO_MIN  4

//Sine Wave related
unsigned int runAC;
#define MAXSPWM     0xFF - DEADTIME
#define MINSPWM     DEADTIME
#define AMPSPWM     128
#define LENGTHSPWM  521
unsigned int PointerSPWM;
int outSPWM;
#define MAXTIMECOUNTAC 2
#define POINTERSTEP 4

// State machine and protection related
#define TRUE 1
#define FALSE 0
unsigned char switchstate[5];
unsigned int Run_state;
int climb;
#define INCREASING 1
#define DECREASING -1
#define CLIMB_STEP 1
