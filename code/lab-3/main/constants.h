/* 
Code for ECE 512 Labs 3-7
Date updated: 2019-05-23
For detailed documentation and explanation please visit https://gitlab.com/agm/ece512
*/

// Interrupt related 
#define SETBIT(ADDRESS,BIT) (ADDRESS |= (1<<BIT))
#define CLEARBIT(ADDRESS,BIT) (ADDRESS &= ~(1<<BIT))
#define CHECKBIT(ADDRESS,BIT) (ADDRESS & (1<<BIT))
#define TOGGLEBIT(ADDRESS,BIT) (ADDRESS ^= (1<<BIT))

//PWM related
unsigned int runDC;
#define PERIOD0     400   // 25kHz:320; 20kHz:400
#define DUTYRATIO0   200
#define PERIOD1     160   // 25kHz:320; 20kHz:400
#define DUTYRATIO1  48
#define DUTYRATIO2  112
#define DEADTIME     16
#define MAXTIMECOUNTDC 2000



// State machine and protection related
#define TRUE 1
#define FALSE 0
unsigned char switchstate;
unsigned int Run_state;
