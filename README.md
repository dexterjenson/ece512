# ECE 512 Power Electronics Laboratory 

The aim of this repository is to help students taking ECE 512 to understand the basics of PEGO, Arduino timers and PWM, and PEGO code. 

**Quick Links**

- [PEGO basics](README.md#pego-basics)  
- [Timer 101](README.md#timer-101) 
- [PWM modes](README.md#timer-operating-modes) 
- [Important ports and pins on PEGO](README.md#important-ports-and-pins-on-pego)
- [Understanding PEGO code](README.md#understanding-pego-code) 
- [Updated Lab notes](README.md#lab-notes-bug-fixes-clarifications)
- [Miscellaneous Topics](README.md#miscellaneous-topics)
	* [PEGO startup technique](README.md#pego-startup-technique)
	* [Compare Match Modes](README.md#compare-match-modes)
  * [Sinewave Lookup Table](README.md#sinewave-lookup-table)
  * [Modulation Index](README.md#modulation-index)

---

### Code

The code can be found in `code/lab`

**This is a generic template. Please modify this for each lab as per the instructions in lab notes**

---

### Lab Notes Updates

See section on [Lab Notes Bug Fixes / Clarifications](README.md#lab-notes-bug-fixes-clarifications) for updated Lab notes

---


### Found a Bug / Need more info / Want to request a new feature?
Refer to [BUGS.md](BUGS.md) to report bugs and get fixes. 

---

### PEGO Basics 
ECE 512 uses the PEGO platform that allows for rapid prototyping of power electronics systems. 

The main power conversion stages for ECE 512 which uses PEGO are as follows 

- 115/60Hz to 16.5V/60Hz transformer
- 16.5V/60Hz to 20V/dc rectifier
- 20V/dc to 12V dc battery charging controller
- 12V dc energy storage battery
- 12V/dc to 12V/ac high frequency inverter
- 12V/ac to 180V/ac high frequency ac transformer
- 180V/ac to 180V dc high frequency rectifier
- 180V/dc to 115V/60Hz low frequency inverter.

---
### Timer 101

Arduino Uno has 3 timers - timer 0, 1 and 2.

- Timers 0 and 2 are 8 bit which means they can count up to 255
- Timer 1 is 16 bit and it count up to 65535
- The clock source for the timers is tied to the frequency of the Arduino which in our case is 16MHz. 

##### TOP, BOTTOM, AND MAX
All timers have 3 associated registers than can used for changing the PWM frequency

- TOP - defines the max value that the timer can count. This is user defined (like we did in Lab 3 for timer 1) or can be a maximum value (like 0xff for timer 2)
- BOTTOM - this is the minimum value and is usually 0
- MAX - the max capability of the timer. For 8 bits it is 255 and 16 bits it is 65535

##### Prescalers 
We use prescalers to have a much larger choice of frequencies. 
Timer resolution = (Prescaler / Input frequency)

For Undo, the input frequency = 16Mhz

| Prescaler value | Timer resolution |
| ------ | ------ |
| 1 | 62.5 ns |
| 8 | 0.5 us|
| 64 | 4 us |
| 256 | 16 us |
| 1024 | 64 us |

Let's say we want to toggle a led at 2 Hz using timer 1 (16 bits). If we use a prescaler value of 1, we can only measure up to `0.000625 ns * 65535 ` which is `~4ms`. This does not meet our 2 Hz requirement. To reduce the speed of the clock fed to the timer, we use prescalers. 

- If we pick the 64 prescaler, the max value we can measure up to is`4 us * 65535` which gives is `~260ms`. We want 500ms period. 
- Lets try the 256 prescaler. The max value we can measure up to is `16 us * 65535` which gives is `~1.05s` which meets our requirement. We can now figure out the timer count we need to check against to detect when the `500ms` is reached. 
---

### Timer Registers 

*TCNTn*- Timer/Counter Register. The actual timer value is stored here. 

*OCRnA/B* - Output Compare Register

*TCCRnA/B* - Timer/Counter Control Register. The prescaler can be configured here. 

*TIMSKn* - Timer/Counter Interrupt Mask Register. This is used to enable/disable timer interrupts.

*TIFRn* - Timer/Counter Interrupt Flag Register. Indicates a pending timer interrupt. 

*ICR* - Input Capture Register (only for 16 bit timers)

---

### Timer Operating Modes 

We have four modes 

- Normal 
- CTC (Clear Timer on Compare Match)
- Fast PWM
- Phase correct PWM

#### Normal mode
- The counting direction is always up
- Wraps around at the TOP value 
- Useful for generating interrupts every N time units 
- Useful for generating interrupts in N time units 

#### CTC mode

**Note** - The ICR register exists only in Timer 1

In CTC mode, the timer counts up to a pre-defined value, generates an interrupt, resets itself and then starts counting again from 0. 

- Timer increments 
- When the counter value (TCNTn) matches either the OCRnA or ICR (for timer 1 only) value the counter resets. 
- The OCRnA or the ICR register is used to define the TOP value 
- When it resets, it starts at 0
- An interrupt can be generated each time the counter value reaches the TOP value by using the OCFnA Flag. If the interrupt is enabled, the interrupt handler routine can be used for updating the TOP value
- For generating a waveform output in CTC mode, the OC2A output can be set to toggle its logical level on each compare match by setting the Compare Output mode bits to toggle mode


<img src="images/ctc.JPG" alt="drawing" width="650"/>

In the above figure, it can be seen that when it is in toggle mode, the TOP value can be changed dynamically to vary the period of the wave. 

**Fast PWM vs Phase Correct PWM**

![Phase vs Fast PWM](images/pwm-fast-phase.gif?raw=true)

*Image Courtesy:https://hekilledmywire.wordpress.com* 

In the above image fast PWM (denoted by standard PWM), the HIGH part of the wave always start in the same place  but the LOW part will end in an arbitrary place as defined by the duty cycle, this means that the center of the HIGH part of the wave will not be constant. In the phase correct PWM, the centers align.

#### Fast PWM Mode

- Timer increments
- Provides high frequency waveform generation output 
- Single slope operation  
- For Timer 1, the PWM frequency can be defined by setting the ICR register or the OCR1A. 
- For Timer 0 and 2, the PWM frequency is fixed at 0xFF or  can be assigned a custom value using the OCRnA register. In many cases, we might be using the OCRnA register as PWM output which leaves us with the fixed 0xFF setting. 



<img src="images/fast-PWM.JPG" alt="drawing" width="650"/>

#### Phase Correct PWM Mode 

- Counter counts from BOTTOM to TOP and then from TOP to BOTTOM
- For Timer 1, the PWM frequency can be defined by setting the ICR register or the OCR1A. 
- For Timer 0 and 2, the PWM frequency is fixed at 0xFF or  can be assigned a custom value using the OCRnA register. In many cases, we might be using the OCRnA register as PWM output which leaves us with the fixed 0xFF setting. 
- In non-inverting compare output mode, the output compare (OCnX) is cleared on the compare match between the TCNTn and OCRnX while upcounting and set when down counting. In inverting output mode, it is inverted. 


<img src="images/phase-correct.JPG" alt="drawing" width="600"/>
----

More info on what these registers mean and how they can be modified is explained through the sample code in the *Understanding PEGO Code Section*


## Important ports and pins on PEGO

<img src="images/arduino-timer.jpg" alt="drawing" width="500"/>

*Image Courtesy: http://reso-nance.org/*

- Timer 0: pins 5(OC0B) and 6(OC0A) on the Arduino 
- Timer 1: pins 9(OC1A) and 10(OC1B) on the Arduino
- Timer 2: pins 3(OC2B) and 11(OC2A) on the Arduino 

**Mapping between Arduino Pins and PEGO Arduino Block**

Timer | Register | Arduino | PEGO Test Pins| PEGO Notation| Application in PEGO | Comments | 
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | 
| Timer 0 | OC0B | Pin 5  | TP24 | PWM_12_A  | State Machine | - |
| Timer 0 | OC0A | Pin 6  | TP26 | PWM_12_B  | State Machine | - |
| Timer 1 | OC1A | Pin 9  | TP17 | PWM_180_A | PWM Generation in the 12V DC to 180V DC stage | Used by the high frequency 12V DC - 12V AC inverter | 
| Timer 1 | OC1B | Pin 10 | TP19 | PWM_180_B | PWM Generation in the 12V DC to 180V DC stage | Used by the high frequency 12V DC - 12V AC inverter | 
| Timer 2 | OC2B | Pin 3  | TP20 | PWM_TEST_A | sPWM Generation in the 180V DC to 120V AC stage | Used by the low frequency 180V DC - 115V/60Hz sine inverter | 
| TImer 2 | OC2A | Pin 11 | TP21 | PWM_TEST_B | sPWM Generation in the 180V DC to 120V AC stage | Used by the low frequency 180V DC - 115V/60Hz sine inverter | 

## Understanding PEGO Code 
**This is a generic template. Please modify this for each lab as per the instructions in lab notes**

The code described here can be found in `code/lab/main/main.ino` and will be explained in sections. 

```c
#include <avr/io.h>
#include <avr/interrupt.h>
#include "constants.h"
#include "sinetable.h"

```

We will be using `<avr/io.h>` and `<avr/interrupt.h>` to for accessing registers and generating interrupts. We are also using two custom files `constants.h` for defining the constants such as period, duty ratio, deadtime etc. and `sinetable.h` for defining the values used in the sPWM generation. 

### Basic Setup and Init

```c
void setup() {
  // ------------------------------------------------------------------------------------------------------
  // Basic steps
  // ------------------------------------------------------------------------------------------------------
  cli();                                       // Disable all interrupts

  GTCCR |= (1 << TSM) | (1 << PSRASY) | (1 << PSRSYNC); // Halt all Timers

  //  ------------------------------------------------------------------------------------------------------
  //   Initial values
  //  ------------------------------------------------------------------------------------------------------
  Run_state = 0;
  runDC = 0;
  runAC = 0;
  climb = 1;
  dutyratio = DUTYRATIO;
  PointerSPWM = 0;

```
The `setup()` function is the standard Arduino function mainly used to initialize I/O, timers etc. We will use it to initialize all 3 timers. While we initialize the timers, we need to disable all the timers. Once we finish the setup, we can enable them. 

Timer 0 will be used to run the state machine. 

**Why state machine?**
- Having a state machine helps us to make the execution more deterministic and also sync different components. 
- We are using Timer 0 as the state machine which runs at a much lower frequency when compared to the actual PWM waveforms. 
- Detecting change in switch state, setting duty cycle values etc. will happen in the state machine. 

### Timer 0 Settings for State Machine

```c
  // --------------------------------------------------------------------------------------------------------
  // Timer 0 settings
  // --------------------------------------------------------------------------------------------------------
  TCCR0A = 0;                           // Reset Timer 0 Control Register A
  TCCR0B = 0;                           // Reset Timer 0 Control Register B
  TCCR0A |= (1 << COM0A1) |             // Clear OC0A on compare match up counting, Set on down counting
            (0 << COM0A0) |
            (1 << COM0B1) |             // Clear OC0B on compare match up counting, Set on down counting
            (0 << COM0B0) |
            (1 << WGM00)  |             // Fast PWM mode, 8-bit TOP = 0xFF
            (1 << WGM01)  ;
  TCCR0B |= (0 << WGM02)  |
            (0 << CS02)   |             // Prescalar N = 8, clock frequency = 16MHz
            (1 << CS01)   |
            (0 << CS00)   ;

```
#### Setting Compare Output Mode

Now that we have set Timer 0 in Fast PWM mode, we should define when the set and rest of the compare register happens. We can do this by setting COM0A0, COM0A1, COM0B0 and COM0B1 registers.

We have the following config 

- COM0A1 = 1  
- COM0A0 = 0
- COM0B1= 1  
- COM0B0 = 0

<img src="images/timer0-com.JPG" alt="drawing" width="600"/>

<img src="images/timer0-comB.JPG" alt="drawing" width="600"/>

You can see that we are using non inverting mode. Also, the OC0A and OC0B registers are being cleared on match and set at the BOTTOM. 

#### Setting Waveform Generation Mode 

The counting sequence is determined by the setting of the WGM01 and WGM00 bits located in the Timer/Counter Control Register (TCCR0A) and the WGM02 bit located in the Timer/Counter Control Register B (TCCR0B). There are close connections between how the counter behaves (counts) and how waveforms are generated on the Output Compare outputs OC0A and OC0B

We can set the Waverform generation mode (phase correct PWM, fast PWM etc.) using WGM02, WGM01 and WGM00 bits in the TCCR0B and TCCR0A registers. 

**In our case, we are setting Timer 0 to fast PWM mode which is mode 3** by setting the following registers 

- WGM00 = 1
- WGM01 = 1
- WGM02 = 0


<img src="images/waveform-timer0.JPG" alt="drawing" width="600"/>

This can be found in Table 14-8 on page 108 of the datasheet. 

Based on our setting we will be using Mode 3. Note that in this mode, the update of OCRx happens at the BOTTOM. Always kep a lookout for when this happens across different timers. 

#### Setting Prescalers 

The CS00, CS01, and CS02 bits in the TCCR0B register can be used to set the prescaler. Here we choose a prescaler of 8. 


<img src="images/prescaler-timer0.JPG" alt="drawing" width="600"/>

#### Setting PWM frequency 
- In the previous section, we set the timer 0 to Mode 3. This means that the TOP value (which determines te PWM frequency) is set to 0xFF. This is fixed and cannot be changed. 
- In Mode 3, timer 0 will run at a fixed frequency. 
- If you want to set the frequency dynamically, you can use Mode 7 using OCR0A. However, in our case this is not possible since OCR0A is being used as a PWM output. 
- We will stick to 0xFF. 

#### Calculating PWM frequency
The PWM frequency can be calculated by 

*PWM frequency for fast PWM mode = (Clock frequency / 256 / N)*

where clock frequency = 16MHz, N = prescaler = 8 (for timer 0)

Doing the math, the PWM frequency for timer 0 is around 7.81 kHz. **Our state machine runs at a frequency of 7.81kHz**

Note that, the above formula changes for different mode/timers.

#### Setting Duty Ratio


```c
  OCR0A = DUTYRATIO;                    // Timer 0A duty cycle
  OCR0B = DUTYRATIO;                    // Timer 0B duty cycle

  TIMSK0 = 0;                           // Reset Timer 0 interrupt mask register
  TIMSK0 |= (1 << OCIE0A) ;             // Timer 0 overflow interrupt TOV0 bit set when TCNT0 = BOTTOM
  TIFR0 = 0;                            // Reset Timer 0 interrupt flag register
  TCNT0 = 0x00;                         // Set Timer 0 counter to 0

```

We are setting the duty ratio by assigning values to the OCR0A and OCR0B registers as per the pre-defined value in `constants.h`. 


----

### Timer 1 Settings for high frequency inverter

```c
// ------------------------------------------------------------------------------------------------------
// Timer 1 settings
// -----------------------------------------------------------------------------------------------------
  TCCR1A = 0;                           // Reset Timer 1 Control Register A
  TCCR1B = 0;                           // Reset Timer 1 Control Register B
  TCCR1A |= (1 << COM1A1) |             // Clear OC1A on compare match up counting, Set on down counting
            (0 << COM1A0) |
            (1 << COM1B1) |             // Set OC1B on compare match up counting, Clear on down counting
            (1 << COM1B0) |
            (0 << WGM10)  |             // Phase & frequency correct mode, 8-bit TOP = ICR1
            (0 << WGM11)  ;
  TCCR1B |= (0 << WGM12)  |
            (1 << WGM13)  |
            (0 << CS12)   |             // Prescalar N = 1, clock frequency = 16MHz
            (0 << CS11)   |
            (1 << CS10)   ;

```

#### Setting Compare Output Mode

We use the Timer 1 in phase and frequency correct PWM mode. Similar to the timer 0, we should define when the set and rest of the compare register. We can do this by setting COM1A0, COM1A1, COM1B0 and COM011 registers.

We have the following config 

- COM1A1 = 1  
- COM1A0 = 0
- COM1B1= 1  
- COM1B0 = 0

Referring to Table 15-3 on page 135 of the datasheet, we see that the following setting has been applied 
- Clear OC1A/OC1B on Compare Match when upcounting. 
- Set OC1A/OC1B on Compare Match when downcounting.

#### Setting Waveform Generation Mode 

The waveform generation mode can be set using bits WGM13, WGM12, WGM11 and WGM10 spread across registers TCCR1A and TCCR1B. 

Referring to Table 15-4 on Page 136 


<img src="images/timer1-wave.JPG" alt="drawing" width="600"/>


**We are setting the timer 1 to phase and frequency correct mode which is mode 8** using the following setting

- WGM10 = 0
- WGM11 = 0
- WGM12 = 0
- WGM13 = 1

With this setting the TOP value is set to the value of ICR1 and the update of OCR1x happens at the BOTTOM (which is zero).

#### Setting Prescalers 

Using bits CS10, CS11, CS12 we set the prescaler to 1. Refer to table 15-5 on page 137 for more details.


#### Setting PWM frequency 

Timer 1 has a special register ICR1 which can be used to set the PWM frequency. This register only exists in the timer 1. We will use this register to set the PWM frequency. The value of ICR1 is defined in the `constants.h` file. 

#### Calculating PWM frequency
The PWM frequency can be calculated by 

*PWM frequency for phase and frequency correct mode = (Clock frequency / TOP / N/2)*

where clock frequency = 16MHz, N = prescaler = 1 (in this specific case for timer 1)

#### Setting Duty Ratio and Deadtime

```c

  ICR1 = PERIOD;
  OCR1A = DUTYRATIO - DEADTIME;         // Timer 1A duty cycle
  OCR1B = ICR1 - OCR1A                  // Timer 1B duty cycle

  TIMSK1 = 0;                           // Reset Timer 1 interrupt mask register
  TIFR1 = 0;                            // Reset Timer 1 interrupt flag register
  TCNT1 = 0x00;                         // Set Timer 1 counter to 0

```

We are setting the duty ratio and deadtime by assigning values to the OCR1A and OCR1B registers as per the pre-defined value in `constants.h`. 

----

### Timer 2 Settings for low frequency inverter

```c
// --------------------------------------------------------------------------------------------------------
// Timer 2 settings
// --------------------------------------------------------------------------------------------------------
TCCR2A = 0;                           // Reset Timer 2 Control Register A
TCCR2B = 0;                           // Reset Timer 2 Control Register B
TCCR2A |= (1 << COM2A1) |             // Clear OC2A on compare match up counting, Set on down counting
          (0 << COM2A0) |
          (1 << COM2B1) |             // Set OC2B on compare match up counting, Clear on down counting
          (1 << COM2B0) |
          (1 << WGM20)  |             // Phase correct mode, 8-bit TOP = 0xFF
          (0 << WGM21)  ;
TCCR2B |= (0 << WGM22)  |
          (0 << CS22)   |             // Prescalar N = 1, clock frequency = 16MHz
          (0 << CS21)   |
          (1 << CS20)   ;

```

#### Setting Compare Output Mode

We use the Timer 2 in phase correct PWM mode. Similar to the timer 0 and 1, we should define when the set and rest of the compare register. We can do this by setting COM1A0, COM1A1, COM1B0 and COM011 registers.

We have the following config 

- COM2A1 = 1  
- COM2A0 = 0
- COM2B1= 1  
- COM2B0 = 0

Referring to Table 17-4 and 17-7 on page 159 and 160 respectively. We can see that the following setting has been applied 
- Clear OC2A on Compare Match when up-counting. Set OC2A on Compare Match when down-counting
- Clear OC2B on Compare Match when up-counting. Set OC2B on Compare Match when down-counting

#### Setting Waveform Generation Mode 

The waveform generation mode can be set using bits WGM13, WGM12, WGM11 and WGM10 spread across registers TCCR1A and TCCR1B. 

Referring to Table 15-4 on Page 136 


<img src="images/timer2-wave.JPG" alt="drawing" width="600"/>


**We are setting the timer 2 to phase correct mode which is mode 1** using the following setting

- WGM20 = 1
- WGM21 = 0
- WGM22 = 0

With this setting the TOP value is set to the value 0xFF and the update of OCR2x happens at the TOP

#### Setting Prescalers 

Using bits CS20, CS21, CS22 we set the prescaler to 1. Refer to table 17-9 on page 162 for more details.

- CS20 = 1
- CS21 = 0
- CS22 = 0

#### Setting PWM frequency 

- In the previous section, we set the timer 1 to mode 1. This means that the TOP value (which determines te PWM frequency) is set to 0xFF. This is fixed and cannot be changed. 
- In Mode 1, timer 2 will run at a fixed frequency. 
- If you want to set the frequency dynamically, you can use Mode 5 using OCR2A. However, in our case this is not possible since OCR2A is being used as a PWM output. 
- We will stick to 0xFF.  

#### Calculating PWM frequency
The PWM frequency can be calculated by 

*PWM frequency for phase correct mode in timer 2 = (Clock frequency / N / 512)*

where clock frequency = 16MHz, N = prescaler = 1 (in this specific case for timer 2)

**In our case this is 31.25 kHz**

#### Setting Duty Ratio and Deadtime

```c

  OCR2A = DUTYRATIO - DEADTIME;         // Timer 2A duty cycle
  OCR2B = 0xFF - OCR2A;                 // Timer 2B duty cycle

  TIMSK2 = 0;                           // Reset Timer 2 interrupt mask register
  TIFR2 = 0;                            // Reset Timer 2 interrupt flag register
  TCNT2 = 0x00;                         // Set Timer 2 counter to 0

```

We are setting the duty ratio and deadtime by assigning values to the OCR2A and OCR2B registers as per the pre-defined value in `constants.h`. 

----

### Setting Ports 

```c
ASSR = 0; // Reset Async status register, TIMER2 clk = CPU clk
  
  // --------------------------------------------------------------------------------------------------------
  // IO settings
  // --------------------------------------------------------------------------------------------------------
  SREG = 0x00;                // Reset AVR status
  SREG |= (1 << 7) ;          // Enable global Interrupt

  GTCCR = 0;                  // Release all timers

  MCUCR = 0;                  // Reset MCU control register
  MCUCR |= (1 << PUD);        // Disable pull-up for all IO pins

  DDRB = 0;                   // Reset data direction register Port B, all input
  DDRD = 0;                   // Reset data direction register Port D, all input
  DDRC = 0;                   // Reset data direction register Port C, all input

  // Digital pins
  DDRD  |= (1 << DDD2) |      // Green LED
           (1 << DDD4) |      // Red LED
           (1 << DDD6) |      // Timer 0A Pin set as output
           (1 << DDD5) ;      // Timer 0B Pin set as output
  DDRB  |= (1 << DDB1) |      // Timer 1A Pin set as output
           (1 << DDB2) ;      // Timer 1B Pin set as output

  PORTD |= (1 << PORTD4);     // Red LED high (off)
  PORTD |= (1 << PORTD2);     // Green LED high (off)

  sei();                      // Enable all interrupts
}

```
In the above section we set the ports. This is self explanatory. 

### State Machine 

``` c
ISR(TIMER0_COMPA_vect) {

```
- As previously stated we will be using timer 0 to run our state machine. 
- The state machine syncs different sections and helps make the process more deterministic
- Instead of using constantly polling the timer count to decide what to do, we use interrupts. An interrupt is a generated for example when the timer reaches a specific count. 
- It gets served using a special function called interrupt service routine (ISR)
- ISR is called by the microcontroller.
- The `ISR(TIMER0_COMPA_vect)` is the ISR for timer 0

#### Keeping track of time taken by the state machine 

It is crucial we track the time it takes for the entire state machine. Since it runs as an ISR it is critical to keep it clean and efficient. 
To track the time it takes to execute one cycle of the state machine, we use a led to measure time using a scope. 

**Led used** - `STATUS_LED_1` (D4 on the schematic sheet 16)  connected to TP 18 on the PEGO block or IO2 on the Arduino

``` c
PORTD &= ~(1 << PORTD2); // Computational load indicator: low. We are turnin on the led

```
**It should be noted that:** 

- Outputing a 1 (high) on the digital pin turns off the green led 
- Outputing a 0 (low) on the digital pin turn on the green led

**Also, the turn on/off action will only be visible on a scope**

#### Starting/Stopping PWM

```c

//   PWM off by 180V_SW
if (CHECKBIT(switchstate[0], 0)) {   // If 180V_SW high, stop pwm
  PORTD |= (1 << PORTD4);            // Red Light off

  DDRB &= ~(1 << DDB3) ;            // Disable PWM2A  
  DDRD &= ~(1 << DDD3) ;            // Disable PWM2B  

  DDRB &= ~(1 << DDB1);            // Disable PWM1A  
  DDRB &= ~(1 << DDB2) ;           // Disable PWM1B  

  Run_state = 0;
  runAC = 0;
  runDC = 0;
  PointerSPWM = 0;

  } else if (Run_state == 0) {   // If 180V_SW low and idle status, start pwm
  PORTD &= ~(1 << PORTD4);     // Red Light on

  DDRB  |= (1 << DDB3) ;       // Enable PWM2A
  DDRD  |= (1 << DDD3) ;       // Enable PWM2B

  DDRB  |= (1 << DDB1) ;      // Enable PWM1A
  DDRB  |= (1 << DDB2) ;      // Enable PWM1B

  Run_state = 1;
  runDC = 1;
  runAC = 1;
  }

```

**We will use the switch `180V_SW` to turn on/off PWM signals for both high low and frequency inverter**

**Switch used** - `180V_SW` (SW2 on the schematic sheet 16)  connected to TP 15 on the PEGO block or IO8 on the Arduino


#### sPWM generation

```c
//   Sine Wave update
if (runAC == 1) {
  outSPWM = (sinetable522[PointerSPWM]);
  if (outSPWM > MAXSPWM) {
     outSPWM = MAXSPWM;
  }
  else if (outSPWM < MINSPWM) {
    outSPWM = MINSPWM;
  }
  OCR2A = outSPWM - DEADTIME;
  OCR2B = outSPWM + DEADTIME;
  PointerSPWM += POINTERSTEP;
  if (PointerSPWM > LENGTHSPWM) {
    PointerSPWM = 0;
  }
}
     
``` 

The above section of code is used to generate the sinusoidal PWM for low frequency inverter (180V DC to 115/60Hz inverter). 

- We are using a lookup table to get the sine values. This is found in ``sinetable.h``.
- Once the PWM switch is enabled and the timer 2 starts, the state machine inside timer 0 picks a value from the sine table and updates the OCR2A and OCR2B registers 

##### What does POINTERSTEP do?

You'll notice that the sine table has 522 values and the timer 2 which is responsible for generating the PWM signal has a frequency of 31.25 kHz. In addition to this there is a variable ``POINTERSTEP`` which has a default value of 4. Looking at the code, you can figure out that it only reads 522/4 values from the sinetable. Why are we doing this?

##### In a world without state machine

Without the state machine, we would have done something like this. We would use the ISR of timer 1 to get values from the sine table and update the OCRnX registers. 

```c
ISR(TIMER2_OVF_vect){
  
  int outSPWM;
  int lengthSPWM;
     
  outSPWM = ( sinetable522[PointerSPWM] );
  lengthSPWM = 521;

  if(outSPWM > MaxSPWM) { outSPWM = MaxSPWM; }
  if(outSPWM < MinSPWM) { outSPWM = MinSPWM; }
   
  OCR2A = outSPWM - DEADTIME;                           
  OCR2B = outSPWM + DEADTIME;                              
   
  PointerSPWM = PointerSPWM + 1;                         
  if(PointerSPWM > lengthSPWM) { PointerSPWM = 0; }    
   
}

```

Now calculating the fundamental frequency

*fundamental_frequency = pwm_frequency/no_of_values_in_sinetable*

We get 31.25kHz/522 ~ 60Hz. This is what we want. 

*Why dont we just do this?*

Updating the duty cycle values async causes issues when you have multiple sections (let's say a PI controller) modifying the duty ratios. For this reason, we want the state machine to update the duty ratio values. 

##### In a world with state machine

Now we want the state machine to update the OCR2A and OCR2B values. However, the state machine runs at around 7.8kHz (timer 0 is set to that frequency). This means that it only pulls value from the sinetable at a frequency of 7.8kHz instead of the 31.25kHz of timer 2. 

To make up for this, we use the `POINTERSTEP` variable with a value of 4. 31.25kHz/4 ~ 7.8kHz. 

Now calculating the fundamental frequency

*fundamental_frequency = pwm_frequency/no_of_values_in_sinetable*

We get 7.8kHz/(522/4) ~ 60Hz. This is what we want. 

#### Task time and Looping 

```c
  PORTD |= (1 << PORTD2); // Computational load indicator: high
}

// ======================================================================================================
// Looping
// ======================================================================================================
void loop() {
  asm ("nop\n\t");

}

```

We turn off the green led. The *turn on - turn off* time of the green led gives us the total time of all the tasks in the state machine. 

----

### Miscellaneous Topics

#### PEGO startup technique
To avoid undefined behavior during startup (especially when you are generating PWM signals), it is advised you turn off the `180V_SW` switch, turn on the main power switch and then turn on the `180V_SW` switch. This ensures the Arduino is first powered on and then the PWM generation is enabled in the state machine. Not doing this sometimes results in an undefined state in microcontroller. 

#### Compare Match Modes
We can set the OCnA pins to be in the inverting or non-inverting mode. This can be done by setting the following registers

- COMnA1
- COMnA0
- COMnB1
- COMnB0

Lets look at these modes for timer 1 (same rules apply for timer 0 and 2)

##### Timer 1

**Fast PWM mode**

What is inverting and non-inverting in Fast PWM mode?
- Non-inverting: Clear (0) OCnX on compare match (TCNTn value matches OCRnX) and set (1) it at the BOTTOM
- Inverting: Set OCnA on compare match and clear it at the BOTTOM

Let's say you want to set OC1A to non-inverting (for one set of switches) and OC1B to inverting. 

- COM1A1 = 1
- COM1A0 = 0
- COM1B1 = 1
- COM1B0 = 1

If you want to set both to non-inverting, 
- COM1A1 = 1
- COM1A0 = 0
- COM1B1 = 1
- COM1B0 = 0


![Timer 1 Non Inverting Mode](images/fast-PWM-compare.JPG?raw=true)

Example of non-inverting mode in fast PWM is shown above. 

Refer to table 15-2 on page 135 for more details. 

**Phase Correct and Phase and Frequency Correct PWM**

*Phase correct and Phase and Frequency Correct PWM modes are both the same if the TOP value is 0xFF*

What is inverting and non-inverting in Phase Correct PWM mode?
- Non-inverting: Clear OCnX on compare match (TCNTn value matches OCRnX) when up counting and set when down counting. 
- Inverting: Set OCnX on compare match (TCNTn value matches OCRnX) when up counting and clear when down counting. 

<img src="images/pwmphase-non.JPG" alt="drawing" width="400"/>

The above image shows both OC1A and OC1B in non-inverting mode. It can be set with 

- COM1A1 = 1
- COM1A0 = 0
- COM1B1 = 1
- COM1B0 = 0

<img src="images/pwmphase-inverting.JPG" alt="drawing" width="400"/>

The above image shows OC1A in non-inverting and OC1B in inverting mode. It can be set with 

- COM1A1 = 1
- COM1A0 = 0
- COM1B1 = 1
- COM1B0 = 1

#### Sinewave Lookup Table

How do you generate a sinewave lookup table?
Lets say we want to generate a table of 522 values. 

**Note: This is just an example to help you understand the concept**

We start with the formula SIN(N * 2 * PI/522 - PI/2) where N goes from 1 - 522. This would give us a wave like below. You can also use SIN(N * 2 * PI/522)

<img src="images/cosine-noOff.png" alt="drawing" width="400"/>

However, we cannot assign negative values. We add an offset. With this, we get


<img src="images/cosine-off.png" alt="drawing" width="400"/>


Now, our timer is a 8bit which means it can take a max value of 255. Scaling it to that, we get 


<img src="images/cosine-multiplier.png" alt="drawing" width="400"/>


*The Y-axis is the value in the sinetable*

#### Modulation Index
As you can see from the previous section, the sine look up table contains an offset. 

To change the modulation index, 
- Figure out the dc offset and the sine value. Lets call these variables *dc_offset* and *sine_value*. 
- Every value in the lookup table is *dc_offset* + *sine_value*.  
- To apply say a modualtion index of 0.5, you have to do *sine_value*/2. 
- Your new entry is *dc_offset* + *sine_value*/2. 
---
### Lab Notes Bug Fixes / Clarifications 

#### Lab 9

Please follow the text below for the lab session 9. The lab notes has errors and wil be updated soon. 

**Chapter 9**

Sinusoidal PWM generation

##### 9.1 Objectives
In this week’s lab we will develop software to perform uniformly sampled digital sine triangle pulse width modulation. We will work with code example and modify the example code to generate complementary PWM outputs at different fundamental frequencies and different carrier frequencies. We can observe the effects of varying the modulation amplitude and sine table sampling rate. Please familiarize yourself with what is in Chapter 12 (interrupts) of the ATMega328P databook posted on the course site and the example code posted on the course site. There are two ways to digitally generate a sine wave calculate the value of the sinusoidal function for each sampling instant using C math libraries, use pre computed sine tables, or to use a cross-coupled quadrature oscillator pair. This lab will use pre computed sine tables to generate the fundamental frequency waveform. The example code uses TIMER2 (PWM_TEST_A and PWM_TEST_B) to produce sinusoidally modulated complementary PWM with carrier frequency of 31.25 kHz (top value of 0xFF) and fundamental frequency of 60 Hz with dead time set at 700 ns. The sine table length is 522. For each interval of the carrier frequency, when the count occurs, the program goes to the sine wave look-up table and picks up a duty-ratio value to be use for that particular switching period. Since we are using the state machine to update the values of the OCR2A and ORC2B registers, they are being updated only at a frequency of 7.8kHz. To account for this we use a factor of 4 (variable PONTERSTP). Thus, when we complete 522/4 such events, we reach the period of 60Hz, because 7800/(522/4) is ~ 60. In this approach the switching frequency and the fundamental frequency are an binary exponent multiple of each other. You may create an alternative sine table with 522 entries, etc. The sine tables have the entries in hexadecimal format.

##### Lab procedure
- Program the Arduino board with the example code. Connect oscilloscope probe to Pin 3 and Pin 11 on the Arduino board and observe the waveform.
- The PWM frequency of timer 2 is set at 31.25kHz using a TOP value of OxFF. Measure the switching frequency and dead time interval.
- With the above setting, measure the fundamental frequency and verify it using the FFT function in the scope.
- Without changing the sine table values, change the fundamental frequency to 15Hz and 30Hz. Verify using the FFT function in the scope.
- Create a new sine table with 256 entries. Use this to create a fundamental frequency of 60Hz. The sinetable can be computed offline using Excel or MATLAB. 
- With the new sine table change the modulation index to 0.5,1 and 2 and observe the relative amplitude of the fundamental frequency waveform. What is the effect on the frequency spectrum when the modulation index is greater than 1? Notice that when you change the modulation index, the dc bias of the waveforms have to be appropriately adjusted. Otherwise, you will not have true ac output. You can do this by varying the modulation index in real-time by post-processing your sine wave entries on the fly. 

----
#### Lab 10

**Chapter 10**

Sine wave inverters

In this lab, we will be developing a sine wave inverter, operating from the 12V dc power and delivering the output to LC filter, feeding a lamp load.

##### 10.1 Software
Modify the code from your last lab, to output PWM_TEST_A and PWM_TEST_B (timer 2) that correspond to sinewave modulated complimentary PWM signals at 31.25kHz switching frequency, with adequate deadtime, during the RUN state. Include a scale factor to vary the amplitude of the sine wave, be sure to monitor the time taken to complete the software tasks as you develop the code using the business monitor (Green LED channel).

##### 10.2 Hardware
- Configure the PEGO system to drive the low frequency H-bridge inverter with the sinusoidally modulated PWM gate drive signals from the PEGO Arduino. Let the PEGO inverter be fed from the 12V dc bus, Let the output of the PEGO inverter be connected to a cascaded L-C filter with a shunt connected lamp load across the capacitor. 
- Test the operation of the circuit, first with the sine wave at zero amplitude to ensure all the gate drivers and switches are operating properly. 
- After that, gently increase the amplitude one step at a time, up to about 90% modulation. Record the output voltage waveform and its Fourier spectrum using the oscilloscope to measure the differential voltage across the load. 
- Remove the capacitor at the output to enable observing the current waveform through the inductor indirectly by measuring the voltage across the load.